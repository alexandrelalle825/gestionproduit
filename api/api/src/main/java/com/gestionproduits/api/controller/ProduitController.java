package com.gestionproduits.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gestionproduits.api.model.Produit;
import com.gestionproduits.api.service.ProduitService;

@RestController
public class ProduitController {

	@Autowired
	private ProduitService produitService;
	
	@PostMapping("/produit")
	public ResponseEntity<Produit> saveProduct(@RequestBody Produit produit) {
		return new ResponseEntity<>(produitService.saveProduit(produit) , HttpStatus.CREATED ) ; 
	}
	/*
	public Produit createProduit(@RequestBody Produit p) {
		produitService.saveProduit(p);
		return p;
	}*/
	
	@GetMapping("/produits")
	public ResponseEntity<Iterable<Produit>>  getProduits(){
		return new ResponseEntity<>(produitService.getProduits() , HttpStatus.OK) ;  
	}
	/*public Iterable<Produit> getProduits(){
		return produitService.getProduits();
	}*/
	
	@GetMapping("/produit/{id}")
	public ResponseEntity<Optional<Produit>> getProduit(@PathVariable("id") final long id) {
		return new ResponseEntity<>(produitService.getProduit(id) , HttpStatus.OK) ;  
	}
	/*
	public Optional<Produit> getProduit(@PathVariable final long id){
		return produitService.getProduit(id);
	}*/
	
	@DeleteMapping("/produit/{id}")
	public void deleteProduit(@PathVariable("id") final long id) {
		produitService.deleteProduit(id);
	}
	
	
	@PutMapping("/produit/{id}")
	public Produit updateProduit(@RequestBody Produit produit, @PathVariable final long id) {
		Optional<Produit> p= produitService.getProduit(id);
		
		if(p.isPresent()) {
			Produit currentProduct= p.get();
			
			if(produit.getNomProduit() != null) {
			currentProduct.setNomProduit(produit.getNomProduit());
			}
	
		currentProduct.setPrix(produit.getPrix());
		
		produitService.saveProduit(currentProduct);
		return currentProduct;
		}
		else
		return null;		
	}
}
