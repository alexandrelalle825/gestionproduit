package com.gestionproduits.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.gestionproduits.api.model.Produit;

public interface ProduitRepository extends CrudRepository<Produit, Long> {

}
