package com.gestionproduits.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gestionproduits.api.model.Produit;
import com.gestionproduits.api.repository.ProduitRepository;

@Service
public class ProduitService {
	
	@Autowired
	ProduitRepository produitRepository;
	
	public Iterable<Produit> getProduits() {
		return produitRepository.findAll();
	}
	
	public Optional<Produit> getProduit(final long id) {
		return produitRepository.findById(id);
	}
	
	public void deleteProduit(final long id) {
		produitRepository.deleteById(id);
	}
	
	public Produit saveProduit(Produit p) {
		Produit savedProd = produitRepository.save(p);
		return savedProd;
	}
	

}
