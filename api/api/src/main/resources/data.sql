	-- table produits
DROP TABLE IF EXISTS produits;
CREATE TABLE produits(
	id SERIAL PRIMARY KEY,
	nomproduit VARCHAR(30) NOT NULL,
	prix numeric(6,2) 
);

INSERT INTO produits (nomproduit,prix) VALUES 
('chemise',149.99), 
('ecran',5000.00),
('disque dur',9500.00), 
('boisson',9.00), 
('classeur',29.90);